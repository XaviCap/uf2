package Tables;

import java.util.ArrayList;
import java.util.Date;


public class Programa {
	
	public static Magatzem elMeuMagatzem = new Magatzem(new ArrayList<Producte>(), new ArrayList<Client>(), new ArrayList<Comanda>(), new ArrayList<Proveidor>());

	
	public static void main(String[] args) {
		//1.- Generaci� d'un magatzem petit�
		generarDadesBasiques(elMeuMagatzem);
		//JComanda jcom = new JComanda(elMeuMagatzem.getComandes());
		//JComanda2 jcom = new JComanda2(elMeuMagatzem.getComandes());
		//jcom.setVisible(true);
		
		
		System.out.println("Veure Magatzem:");
		System.out.println(elMeuMagatzem);
		
		//2.- Veure la composici� dels productes del magatzem
		for(Producte p: elMeuMagatzem.getProductes()) 
			if (p.getTipus() == Tipus.VENDIBLE) 
				System.out.println(p.veureComposicio());
	
		//3.- Veure magatzem ordenat per nom producte
		elMeuMagatzem.getProductes().sort(null);
		System.out.println("3.- Magatzem ordenat per nomProducte");
		System.out.println(elMeuMagatzem);
		
		//4.- Veure magatzem ordenat per stock
		
		System.out.println("3.- Magatzem ordenat per Stock");
		elMeuMagatzem.getProductes().sort(new CompararStock());
		System.out.println(elMeuMagatzem);	
	}

	private static void generarDadesBasiques(Magatzem mgz) {
		
		//Prove�dors
		Proveidor pv1 = new Proveidor(12342, "UNOproveidor", "124213", true, "Manolo");
		
		Proveidor pv2 = new Proveidor (12313, "DOSproveidor", "af3ra", true, "Manolo");
		
		mgz.getProveidors().add(pv1);
		mgz.getProveidors().add(pv2);
		
		// Productes, composici� i lots
		Producte pliv = new Producte("pLiviano", UnitatMesura.UNITAT,4);
		pliv.setProveidor(pv1);
		Producte pllim = new Producte("pLLimona", UnitatMesura.UNITAT,6);
		pllim.setProveidor(pv1);

	    Date dataCaducitat;
		Producte p = new Producte("sucre",UnitatMesura.GRAMS,100000);
	    
	    dataCaducitat = Tools.sumarDies(new Date(), 10);
		p.afegirLot(40000,dataCaducitat);
		p.afegirLot(30000, dataCaducitat);		
		dataCaducitat = Tools.sumarDies(dataCaducitat,20);
		p.afegirLot(70000, dataCaducitat);
		
		p.setProveidor(pv1);
		mgz.add(p);
		
		pliv.afegirComponent(p, 115);
		pllim.afegirComponent(p, 4);
		
		p = new Producte("ous", UnitatMesura.UNITAT,240);
		p.afegirLot(480, dataCaducitat);
		mgz.add(p);
		
		p.setProveidor(pv1);
		
		pliv.afegirComponent(p, 4);
		
		p = new Producte("farina", UnitatMesura.GRAMS,30000);
		p.afegirLot(10000, dataCaducitat);
		p.afegirLot(20000, dataCaducitat);
		
		p.setProveidor(pv1);
		mgz.add(p);
		
		pliv.afegirComponent(p, 115);
		
		p = new Producte("llevadura", UnitatMesura.GRAMS,5000);
		p.afegirLot(200, (new Date()));
		dataCaducitat = Tools.sumarDies(new Date(), -5);
		p.afegirLot(400, dataCaducitat);
		dataCaducitat = Tools.sumarDies(new Date(), 5);
		p.afegirLot(100, dataCaducitat);
		
		p.setProveidor(pv1);
		mgz.add(p);

		pliv.afegirComponent(p, 10);
		pllim.afegirComponent(p, 8);

		Producte pSec = new Producte("Secret", UnitatMesura.UNITAT,0);
		pSec.setStock(100);
		
		pSec.setProveidor(pv2);

		pliv.afegirComponent(pSec, 1);
		pllim.afegirComponent(pSec, 1);

		p = new Producte("nabius", UnitatMesura.GRAMS,4000);
		dataCaducitat = Tools.sumarDies(new Date(), 15);
		p.afegirLot(2000, dataCaducitat);
		p.setProveidor(pv2);
		mgz.add(p);
		
		pSec.afegirComponent(p, 100);
		mgz.add(pSec);
		
		p = new Producte("llimona", UnitatMesura.GRAMS,4000);
		dataCaducitat = Tools.sumarDies(new Date(), 15);
		p.afegirLot(2000, dataCaducitat);
		
		p.setProveidor(pv2);
		mgz.add(p);
		
		pliv.afegirComponent(p, 40);
		pllim.afegirComponent(p, 4);
		
		p = new Producte("albahaca", UnitatMesura.GRAMS,4000);
		dataCaducitat = Tools.sumarDies(new Date(), 15);
		p.afegirLot(2000, dataCaducitat);
		
		p.setProveidor(pv2);
		mgz.add(p);

		pllim.afegirComponent(pSec, 20);
		
		pliv.setTipus(Tipus.VENDIBLE);
		pllim.setTipus(Tipus.VENDIBLE);
		
		pliv.setPreuVenda(20);
		pllim.setPreuVenda(15);
		pllim.setStock(18);

		mgz.add(pliv);
		mgz.add(pllim);
		
		
		//clients
		Client c1 = new Client("La Canasta", 39.1174353, -5.7933869);
		Client c2 = new Client("Baires", 41.5442476, 2.0604163);
		Client c3 = new Client("Pierre Herme", 48.8513876, 2.3304912);
		Client c4 = new Client("Aux Pains de Papy", 51.5293753, -0.1903852);
		Client c5 = new Client("La Santiaguesa", 40.9284811, -5.2618384);
		mgz.getClients().add(c1);
		mgz.getClients().add(c2);
		mgz.getClients().add(c3);
		mgz.getClients().add(c4);
		mgz.getClients().add(c5);
		
		Comanda m1 = new Comanda(c1);
		m1.getLinies().add(new ComandaLinia (pliv,100,20));
		m1.getLinies().add(new ComandaLinia(pllim,40,12));
		mgz.getComandes().add(m1);

		m1 = new Comanda(c2);
		m1.getLinies().add(new ComandaLinia (pllim,20,15));
		m1.getLinies().add(new ComandaLinia(pllim,4,0));
		mgz.getComandes().add(m1);
		
		m1 = new Comanda(c3);
		m1.getLinies().add(new ComandaLinia (pliv,50,18));
		mgz.getComandes().add(m1);

		m1 = new Comanda(c1);
		m1.getLinies().add(new ComandaLinia (pliv,20,20));
		m1.getLinies().add(new ComandaLinia(pllim,1,0));
		mgz.getComandes().add(m1);
		
	}
	
	
}
