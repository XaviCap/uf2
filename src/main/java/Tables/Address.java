package Tables;

public class Address{
	
	protected boolean principal;
	
	protected String carrer;
	
	protected int numero;
	
	protected String poblacio;
	
	protected String pais;
	
	protected String postal;
	
	protected String telefon;
	
	protected double latitud;
	
	protected double longitud;

    private Client client;
	
	private Proveidor proveidor;

	public Address(String carrer, int numero, String poblacio, String pais, String postal, String telefon,
			double latitud, double longitud) {
		super();
		this.carrer = carrer;
		this.numero = numero;
		this.poblacio = poblacio;
		this.pais = pais;
		this.postal = postal;
		this.telefon = telefon;
		this.latitud = latitud;
		this.longitud = longitud;
	}
	
	

}