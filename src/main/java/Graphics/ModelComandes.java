package Graphics;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import Tables.Comanda;
import Tables.LotDesglossat;
import Tables.Producte;
import Tables.Programa;

public class ModelComandes extends AbstractTableModel{

	ArrayList<Comanda> pr = null;
	private final String[] tableHeaders = { "idComanda", "Client", "Data Comanda","Data Lliurament"};
	
	public ModelComandes(ArrayList p) {
		pr = p;
	}
	
	@Override
	public int getRowCount() {
		return pr.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Comanda entity = null;
		entity = pr.get(rowIndex);

		switch (columnIndex) {
		
		case 0:
			
			return entity.getIdComanda();
			
		case 1:
			
			return entity.getClient().getIdClient();
			
		case 2: 
			
			return entity.getDataComanda();
			
		case 3:
			
			return entity.getDataLliurament();
			
		default:
		
		}

		
		return null;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}
	
}
