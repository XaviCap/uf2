package Graphics;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Tables.Producte;

public class ModelProductos extends AbstractTableModel {

	ArrayList lista;
	private final String[] tableHeaders = { "idProducte", "nomProducte", "stock","stock m�nim","tipus", "Proveidor" };

	public ModelProductos(ArrayList list) {
		lista = list;
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public int getRowCount() {
		return lista.size();
	}

	// this method is called to set the value of each cell
	@Override
	public Object getValueAt(int row, int column) {

		Producte entity = null;
		entity = (Producte) lista.get(row);

		switch (column) {

		case 0:

			return entity.getCodiProducte();

		case 1:

			return entity.getNomProducte();
			
		case 2:
			
			return entity.getStock();
			
		case 3: 
			
			return entity.getStockMinim();
			
		case 4:
			
			return entity.getTipus();

		case 5:
			
			if(entity.getProveidor() == null) {
				
				return "null";
				
			}else {

			return entity.getProveidor().getIdProveidor();

			}
		default:

			return "";
		}
	}

	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}
}
