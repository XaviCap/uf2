package Graphics;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JTextField;

import Tables.Producte;
import Tables.Programa;
import Tables.Proveidor;
import Tables.UnitatMesura;

import java.awt.BorderLayout;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Toolkit;
import java.awt.Color;

public class ModificarProducto {

	private JFrame frmModificarProducte;
	private JTextField Nom;
	private JTextField UnitatMesura;
	private JTextField StockMínim;
	private JTable table_1;
	static Producte pr = null;
	Proveidor proveidor = null;
	private JTextField textField;

	
	public JFrame getFrame() {
		return frmModificarProducte;
	}
	/**
	 * Create the application.
	 */
	public ModificarProducto(Producte p) {
		initialize(p);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Producte p) {
		
		pr = p;
		frmModificarProducte = new JFrame();
		frmModificarProducte.setForeground(new Color(255, 153, 102));
		frmModificarProducte.setTitle("Modificar Producte");
		frmModificarProducte.setIconImage(Toolkit.getDefaultToolkit().getImage(ModificarProducto.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		frmModificarProducte.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmModificarProducte.setBounds(100, 100, 484, 323);
		frmModificarProducte.getContentPane().setLayout(null);
		
		Nom = new JTextField();
		Nom.setColumns(10);
		Nom.setBounds(93, 26, 86, 20);
		Nom.setText(p.getNomProducte());
		frmModificarProducte.getContentPane().add(Nom);
		
		UnitatMesura = new JTextField();
		UnitatMesura.setColumns(10);
		UnitatMesura.setBounds(93, 57, 86, 20);
		UnitatMesura.setText(p.getUnitatMesura().name());
		frmModificarProducte.getContentPane().add(UnitatMesura);
		
		StockMínim = new JTextField();
		StockMínim.setColumns(10);
		StockMínim.setBounds(93, 88, 86, 20);
		StockMínim.setText(""+p.getStockMinim());
		frmModificarProducte.getContentPane().add(StockMínim);
		
		JLabel label = new JLabel("Nom*");
		label.setBounds(10, 29, 46, 14);
		frmModificarProducte.getContentPane().add(label);
		
		JLabel label_1 = new JLabel("Unitat mesura");
		label_1.setBounds(10, 60, 73, 14);
		frmModificarProducte.getContentPane().add(label_1);
		
		JLabel label_2 = new JLabel("Stock m\u00EDnim");
		label_2.setBounds(10, 91, 73, 14);
		frmModificarProducte.getContentPane().add(label_2);
		
		JButton button = new JButton("Guardar\r\n");
		
		button.setBounds(10, 213, 89, 23);
		frmModificarProducte.getContentPane().add(button);
		
		JButton btnClose = new JButton("Tancar");
		btnClose.setBounds(128, 213, 89, 23);
		frmModificarProducte.getContentPane().add(btnClose);
		
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				int n = Integer.parseInt(StockMínim.getText());
				
				p.setNomProducte(Nom.getText());
				p.setUnitatMesura(Enum.valueOf(UnitatMesura.class, UnitatMesura.getText()));
				p.setStockMinim(n);
				p.setProveidor(proveidor);

				
							
			}

		});
		
		btnClose.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				frmModificarProducte.setVisible(false);
			}					
		});
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(200, 26, 243, 155);
		frmModificarProducte.getContentPane().add(scrollPane);
		
		table_1 = new JTable();
		scrollPane.setViewportView(table_1);
		
		initTableProveidor(table_1, (ArrayList) Programa.elMeuMagatzem.getProveidors());
		
		textField = new JTextField();
		textField.setBounds(93, 119, 86, 20);
		frmModificarProducte.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblLot = new JLabel("Lot");
		lblLot.setBounds(10, 122, 46, 14);
		frmModificarProducte.getContentPane().add(lblLot);
		
		table_1.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				int n = table_1.getSelectedRow();

				Object s = table_1.getValueAt(n, 0);
				
				for(Proveidor p : Programa.elMeuMagatzem.getProveidors()) {
					
					if(s.equals(p.getIdProveidor())) {
						
						proveidor = p;
						
					}
					
				}
				


			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}
	

	public static void initTableProveidor(JTable table, ArrayList list) {
		
		ArrayList<Proveidor> a2 = new ArrayList<Proveidor>();

		for(Object p1 : list) {
			
			Proveidor pro = (Proveidor) p1;
			
			if(pro.equals(pr.getProveidor())) {
				
			}else {
				
				a2.add(pro);
				
			}
			
		}
		
		ModelProveidor model = new ModelProveidor(a2);

		table.setModel(model);

	}
}
