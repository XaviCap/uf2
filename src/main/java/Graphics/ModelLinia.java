package Graphics;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Tables.Comanda;
import Tables.ComandaLinia;

public class ModelLinia extends AbstractTableModel{
	
	Comanda c = null;
	private final String[] tableHeaders = { "Producte", "Quantitat", "Preu venda"};
	
	public ModelLinia(Comanda cc) {
		c = cc;
	}
	
	@Override
	public int getRowCount() {
		return c.getLinies().size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		ComandaLinia entity = null;
		entity = c.getLinies().get(rowIndex);

		switch (columnIndex) {
		
		case 0:
			
			return entity.getProducte();
			
		case 1:
			
			return entity.getQuantitat();
			
		case 2: 
			
			return entity.getPreuVenda();
		default:
		
		}

		
		return null;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}

}
