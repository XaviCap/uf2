package Graphics;

import java.awt.EventQueue;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.JFrame;
import javax.swing.JTextField;

import Tables.LotDesglossat;
import Tables.Producte;
import Tables.Programa;
import Tables.Proveidor;
import Tables.Tools;
import Tables.UnitatMesura;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Toolkit;

public class AfegirProducte {

	private JFrame frmAfegirProducte;
	public JFrame getFrame() {
		return frmAfegirProducte;
	}

	public void setFrame(JFrame frame) {
		this.frmAfegirProducte = frame;
	}

	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTable tableProveidor;
	int mouseX = 0;
	int mouseY = 0;
	Proveidor p = null;
	private JTextField textField_4;

	public AfegirProducte() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frmAfegirProducte = new JFrame();
		frmAfegirProducte.setTitle("Afegir Producte");
		frmAfegirProducte.setIconImage(Toolkit.getDefaultToolkit().getImage(AfegirProducte.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		frmAfegirProducte.setBounds(100, 100, 469, 290);
		frmAfegirProducte.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAfegirProducte.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(93, 11, 86, 20);
		frmAfegirProducte.getContentPane().add(textField);
		textField.setColumns(10);

		textField_1 = new JTextField();
		textField_1.setBounds(93, 42, 86, 20);
		frmAfegirProducte.getContentPane().add(textField_1);
		textField_1.setColumns(10);

		textField_2 = new JTextField();
		textField_2.setBounds(93, 73, 86, 20);
		frmAfegirProducte.getContentPane().add(textField_2);
		textField_2.setColumns(10);

		JLabel lblNewLabel = new JLabel("Nom*");
		lblNewLabel.setBounds(10, 14, 46, 14);
		frmAfegirProducte.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Unitat mesura");
		lblNewLabel_1.setBounds(10, 45, 73, 14);
		frmAfegirProducte.getContentPane().add(lblNewLabel_1);

		JLabel lblNewLabel_2 = new JLabel("Stock m\u00EDnim");
		lblNewLabel_2.setBounds(10, 76, 73, 14);
		frmAfegirProducte.getContentPane().add(lblNewLabel_2);

		JButton btnNewButton = new JButton("Afegir\r\n");
		btnNewButton.setBounds(10, 198, 89, 23);
		frmAfegirProducte.getContentPane().add(btnNewButton);

		JButton btnClose = new JButton("Tancar");
		btnClose.setBounds(128, 198, 89, 23);
		frmAfegirProducte.getContentPane().add(btnClose);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(200, 11, 243, 155);
		frmAfegirProducte.getContentPane().add(scrollPane);

		tableProveidor = new JTable();
		scrollPane.setViewportView(tableProveidor);

		initTableProveidor(tableProveidor, (ArrayList) Programa.elMeuMagatzem.getProveidors());
		
		JLabel lblLot = new JLabel("Lots");
		lblLot.setBounds(10, 107, 73, 14);
		frmAfegirProducte.getContentPane().add(lblLot);
		
		textField_4 = new JTextField();
		textField_4.setColumns(10);
		textField_4.setBounds(93, 104, 86, 20);
		frmAfegirProducte.getContentPane().add(textField_4);
		
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				Producte pr = new Producte(textField.getText(), Enum.valueOf(UnitatMesura.class, textField_1.getText()),
						Integer.parseInt(textField_2.getText()));
				
				int n= Integer.parseInt(textField_4.getText());
				
				Date d= Tools.sumarDies(Calendar.getInstance().getTime(), 2);
				
				for(int i=0;i<n;i++) {
				pr.getLots().add(new LotDesglossat(i, d,300));//quantitat arbitrÓria
				}
				Programa.elMeuMagatzem
						.add(pr);
				
				if(!p.equals(null)) {
					
					pr.setProveidor(p);
					
				}
				
			}

		});
		
		tableProveidor.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				mouseX = e.getX();
				mouseY = e.getY();

				int n = tableProveidor.getSelectedRow();

				Object s = tableProveidor.getValueAt(n, 0);

				for (Proveidor p1 : Programa.elMeuMagatzem.getProveidors()) {

					if (s.equals(p1.getIdProveidor())) {

						p = p1;

					}

				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		
		btnClose.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				frmAfegirProducte.setVisible(false);
				
			}					
		});
	}

	public static void initTableProveidor(JTable table, ArrayList list) {

		
		ModelProveidor model = new ModelProveidor(list);

		table.setModel(model);

	}
}
