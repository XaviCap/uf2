package Graphics;

import Tables.Client;
import Tables.Comanda;
import Tables.ComandaEstat;
import Tables.LotDesglossat;
import Tables.Producte;
import Tables.Programa;
import Tables.Proveidor;
import net.bytebuddy.description.modifier.EnumerationState;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.swing.*;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.*;

import java.awt.event.MouseAdapter;
import java.awt.Dimension;
import java.awt.Dialog.ModalExclusionType;
		
		
import Tables.*;
import java.awt.Toolkit;

public class Ventanas extends JFrame implements ActionListener {

	private JPanel contentPane;
	private JTextField ProdNom;
	private JTextField textField_1;
	public Producte p = null;
	public Client c = null;
	Comanda co = null;
	int mouseX = 0;
	int mouseY = 0;
	private JTable tableProducte;
	private JTable tableLot;
	private JTable tableComponent;
	private JTextField comanda_id;
	private JTextField Client_id;
	private JTable tableComandes;
	private JTable tablaLinia;
	private JTextField Nom;
	private JTextField cif;
	private JTextField Id;
	private JTextField Direccio;
	private JTextField poblacio;
	private JTextField contacte;
	private JTextField telefon;
	private JTable tableClients;
	private JTextField dtaLLiu;
	private LotDesglossat l;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Programa.main(args);
					Ventanas frame = new Ventanas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventanas() {
		setIconImage(Toolkit.getDefaultToolkit().getImage(Ventanas.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		setTitle("Gestor del Magatzem");

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 742, 511);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JTabbedPane tabPane = new JTabbedPane();
		tabPane.setBounds(5, 5, 711, 456);
		contentPane.setLayout(null);
		getContentPane().add(tabPane);
		JPanel Productes = new JPanel();

		tabPane.add("Productes", Productes);

		ProdNom = new JTextField();
		ProdNom.setBounds(72, 5, 86, 20);
		ProdNom.addActionListener(this);
		Productes.setLayout(null);
		Productes.add(ProdNom);
		ProdNom.setColumns(10);

		JLabel lblNewLabel = new JLabel("Num. Proveidor\r\n");
		lblNewLabel.setBounds(169, 8, 95, 14);
		Productes.add(lblNewLabel);

		textField_1 = new JTextField();
		textField_1.setBounds(274, 5, 86, 20);
		Productes.add(textField_1);
		textField_1.setColumns(10);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 65, 686, 155);
		Productes.add(scrollPane);

		tableProducte = new JTable();
		scrollPane.setViewportView(tableProducte);

		initTableProducte(tableProducte, (ArrayList) Programa.elMeuMagatzem.getProductes());
	
		JLabel result = new JLabel("Nom\r\n");
		result.setBounds(10, 8, 68, 14);
		Productes.add(result);

		JButton btnBorrar = new JButton("Esborrar");
		btnBorrar.setBounds(607, 394, 89, 23);
		Productes.add(btnBorrar);

		JButton btnAadir = new JButton("Afegir P\r\n\r\n");
		btnAadir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AfegirProducte va = new AfegirProducte();
				va.getFrame().setVisible(true);
				;

			}
		});
		btnAadir.setBounds(410, 394, 89, 23);
		Productes.add(btnAadir);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(10, 267, 270, 126);
		Productes.add(scrollPane_1);

		tableLot = new JTable();
		scrollPane_1.setViewportView(tableLot);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(326, 257, 370, 126);
		Productes.add(scrollPane_2);

		tableComponent = new JTable();
		scrollPane_2.setViewportView(tableComponent);

		JLabel lblLots = new JLabel("Lots");
		lblLots.setBounds(24, 242, 46, 14);
		Productes.add(lblLots);

		JLabel lblComposici = new JLabel("Composici\u00F3");
		lblComposici.setBounds(337, 232, 73, 14);
		Productes.add(lblComposici);

		JLabel lblProductes = new JLabel("Productes");
		lblProductes.setBounds(20, 43, 86, 14);
		Productes.add(lblProductes);

		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(508, 394, 89, 23);
		Productes.add(btnModificar);

		JButton addlot = new JButton("Afegir Lot");
		addlot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					AfegirLot al=new AfegirLot(p);
					al.getFrame().setVisible(true);
				}catch(Exception NullPointerException) {
				if(p.equals(null)) {
					
					AfegirLot al=new AfegirLot(p);
					al.getFrame().setVisible(true);}
				}
				
				
			}
				
		});
		addlot.setBounds(311, 394, 89, 23);
		Productes.add(addlot);

		JPanel Comandes_1 = new JPanel();
		Comandes_1.setLayout(null);
		tabPane.addTab("Comandes", null, Comandes_1, null);

		comanda_id = new JTextField();
		comanda_id.setColumns(10);
		comanda_id.setBounds(88, 5, 86, 20);
		Comandes_1.add(comanda_id);

		JLabel lblClient = new JLabel("\r\nClient");
		lblClient.setBounds(197, 8, 95, 14);
		Comandes_1.add(lblClient);

		Client_id = new JTextField();
		Client_id.setColumns(10);
		Client_id.setBounds(274, 5, 86, 20);
		Comandes_1.add(Client_id);
		
		JButton EsborrarLot = new JButton("Borra Lot");
		EsborrarLot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				p.getLots().remove(l);
				
				initTableLot(tableLot, p);
				
			}
		});
		
		EsborrarLot.setBounds(212, 394, 89, 23);
		Productes.add(EsborrarLot);
		
		tableLot.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				mouseX = e.getX();
				mouseY = e.getY();

				int n = tableLot.getSelectedRow();

				Object s = tableLot.getValueAt(n, 0);

				for (LotDesglossat l1 : p.getLots()) {

					if (s.equals(l1.getLot())) {

						l=l1;	
												
					}

				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setBounds(10, 65, 686, 159);
		Comandes_1.add(scrollPane_3);

		tableComandes = new JTable();
		scrollPane_3.setViewportView(tableComandes);

		JLabel lblIdcomanda = new JLabel("id_comanda");
		lblIdcomanda.setBounds(10, 8, 68, 14);
		Comandes_1.add(lblIdcomanda);

		JButton button = new JButton("Esborrar");
		button.setBounds(607, 394, 89, 23);
		Comandes_1.add(button);

		JButton button_1 = new JButton("Afegir\r\n");
		Comandes_1.add(button_1);

		JLabel Comandes = new JLabel("Comandes pendents");
		Comandes.setBounds(20, 43, 124, 14);
		Comandes_1.add(Comandes);

		initTableComanda(tableComandes, (ArrayList) Programa.elMeuMagatzem.getComandes());

		JScrollPane TablaLinia = new JScrollPane();
		TablaLinia.setBounds(10, 252, 350, 148);
		Comandes_1.add(TablaLinia);

		tablaLinia = new JTable();
		TablaLinia.setViewportView(tablaLinia);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(450, 235, 147, 148);

		// PESTA�A COMANDAS

		Comandes_1.add(panel_1);
		panel_1.setLayout(null);

		JRadioButton Pendent = new JRadioButton("PENDENT");
		Pendent.setBounds(31, 40, 71, 23);
		panel_1.add(Pendent);

		JRadioButton Transport = new JRadioButton("TRANSPORT");
		Transport.setBounds(31, 66, 85, 23);
		panel_1.add(Transport);

		JRadioButton Preparada = new JRadioButton("PREPARADA");
		Preparada.setBounds(31, 92, 85, 23);
		panel_1.add(Preparada);

		JRadioButton Lliurada = new JRadioButton("LLIURADA");
		Lliurada.setBounds(31, 118, 75, 23);
		panel_1.add(Lliurada);

		JLabel lblEstat = new JLabel("Estat");
		lblEstat.setBounds(56, 11, 46, 14);
		panel_1.add(lblEstat);

		JButton ModComanda = new JButton("Modificar");
		ModComanda.setBounds(397, 394, 89, 23);
		Comandes_1.add(ModComanda);

		JLabel lblLiniesDeComanda = new JLabel("Linies de comanda");
		lblLiniesDeComanda.setBounds(10, 229, 124, 14);
		Comandes_1.add(lblLiniesDeComanda);

		JLabel lblDataLliurament = new JLabel("Data Lliurament");
		lblDataLliurament.setBounds(391, 8, 95, 14);
		Comandes_1.add(lblDataLliurament);

		dtaLLiu = new JTextField();
		dtaLLiu.setColumns(10);
		dtaLLiu.setBounds(492, 5, 86, 20);
		Comandes_1.add(dtaLLiu);

		ModComanda.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// modificar data lliurament
				Exception e1 = new Exception("La data de lliurament no es correcta");
				SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
				String dateInString = dtaLLiu.getText();
				Comanda co1 = null;
				try {
					if (dateInString.equals("")) {

					} else {
						Date date = sdf.parse(dateInString);

						co.setDataLliurament(date);
					}
				} catch (Exception ParseException) {
					System.out.println("No s'ha modificat la data, utilitza el format dd/mm/yyyy");
				}

				// cambiar el client que fa la comanda
				try {
					for (Client c : Programa.elMeuMagatzem.getClients()) {

						if (c.getIdClient() == (Integer.parseInt(Client_id.getText()))) {
							co.setClient(c);
							co1 = co;

						}
					}
				} catch (Exception NumberFormatException) {

				}

				initTableComanda(tableComandes, (ArrayList) Programa.elMeuMagatzem.getComandes());

			}

		});
		// PESTA�A CLIENTES

		ArrayList<JTextField> AJF = new ArrayList<>();

		JPanel Clients = new JPanel();
		Clients.setToolTipText("Clients");
		Clients.setLayout(null);
		tabPane.addTab("Clients", null, Clients, null);

		Nom = new JTextField();
		Nom.setColumns(10);
		Nom.setBounds(324, 8, 86, 20);
		Clients.add(Nom);
		AJF.add(Nom);

		JLabel label = new JLabel("CIF");
		label.setBounds(482, 11, 68, 14);
		Clients.add(label);

		cif = new JTextField();
		cif.setColumns(10);
		cif.setBounds(577, 8, 86, 20);
		Clients.add(cif);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4.setBounds(10, 176, 686, 155);
		Clients.add(scrollPane_4);

		tableClients = new JTable();
		scrollPane_4.setViewportView(tableClients);

		JLabel label_1 = new JLabel("Nom\r\n");
		label_1.setBounds(255, 11, 68, 14);
		Clients.add(label_1);

		JButton delete = new JButton("Esborrar");
		delete.setBounds(607, 394, 89, 23);
		Clients.add(delete);

		delete.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int n = tableClients.getSelectedRow();

				Object s = tableClients.getValueAt(n, 1);

				Client c = null;

				for (Client c1 : Programa.elMeuMagatzem.getClients()) {

					if (c1.getNomClient().equals(s)) {

						c = c1;

					}

				}

				Programa.elMeuMagatzem.getClients().remove(c);


				initTableClients(tableClients, (ArrayList) Programa.elMeuMagatzem.getClients());

			}
		});

		JLabel label_2 = new JLabel("Direcci\u00F3");
		label_2.setBounds(10, 60, 46, 14);
		Clients.add(label_2);

		JLabel label_3 = new JLabel("Poblaci\u00F3");
		label_3.setBounds(255, 60, 73, 14);
		Clients.add(label_3);

		JLabel label_4 = new JLabel("Id");
		label_4.setBounds(10, 11, 56, 14);
		Clients.add(label_4);

		JButton modify = new JButton("Modificar");
		modify.setBounds(508, 394, 89, 23);
		Clients.add(modify);

		Id = new JTextField();
		Id.setColumns(10);
		Id.setBounds(77, 8, 86, 20);
		Clients.add(Id);

		Direccio = new JTextField();
		Direccio.setColumns(10);
		Direccio.setBounds(77, 57, 86, 20);
		Clients.add(Direccio);
		AJF.add(Direccio);

		poblacio = new JTextField();
		poblacio.setColumns(10);
		poblacio.setBounds(324, 57, 86, 20);
		Clients.add(poblacio);
		AJF.add(poblacio);

		JLabel label_5 = new JLabel("Contacte");
		label_5.setBounds(477, 60, 73, 14);
		Clients.add(label_5);

		contacte = new JTextField();
		contacte.setColumns(10);
		contacte.setBounds(577, 57, 86, 20);
		Clients.add(contacte);
		AJF.add(contacte);

		JLabel label_6 = new JLabel("Tel\u00E8fon");
		label_6.setBounds(10, 102, 46, 14);
		Clients.add(label_6);

		telefon = new JTextField();
		telefon.setColumns(10);
		telefon.setBounds(77, 99, 86, 20);
		Clients.add(telefon);
		AJF.add(telefon);

		initTableClients(tableClients, (ArrayList) Programa.elMeuMagatzem.getClients());

		JRadioButton actiu = new JRadioButton("actiu");
		actiu.setBounds(255, 99, 96, 20);
		Clients.add(actiu);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(311, 394, 89, 23);
		Clients.add(btnBuscar);

		JButton add1 = new JButton("Afegir\r\n");
		add1.setBounds(410, 394, 89, 23);
		Clients.add(add1);
		add1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Client c2 = new Client(AJF.get(0).getText());
				Programa.elMeuMagatzem.getClients().add(c2);
				if (telefon.getText().equals("")) {

				} else
					c2.setTelefon(telefon.getText());
				if (cif.getText().equals("")) {

				} else {
					c2.setCIF(cif.getText());
				}
				if (Direccio.getText().equals("")) {

				} else {
					c2.setDireccio(Direccio.getText());
				}
				if (poblacio.getText().equals("")) {

				} else {
					c2.setPoblacio(poblacio.getText());
				}
				if (contacte.getText().equals("")) {

				} else {
					c2.setPersonaContacte(contacte.getText());
				}
				if (telefon.getText().equals("")) {

				} else {
					c2.setTelefon(telefon.getText());
				}
				if (actiu.isSelected()) {
					c2.setActiu(true);
				} else {
					c2.setActiu(false);
				}
				initTableClients(tableClients, (ArrayList) Programa.elMeuMagatzem.getClients());
			}
		});
		btnBuscar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				int idc;
				String nomc = Nom.getText();
				if (Id.getText().equals("")) {
					idc = 0;
				} else
					idc = Integer.parseInt(Id.getText());

				ArrayList<Client> list = (ArrayList) Programa.elMeuMagatzem.getClients();

				ArrayList<Client> list1 = new ArrayList<Client>();

				for (int i = 0; i < list.size(); i++) {

					if (list.get(i).getNomClient().equals(nomc) || (list.get(i).getIdClient() == idc)) {

						list1.add(list.get(i));

					}

				}

				if (list1.isEmpty()) {

					initTableClients(tableClients, (ArrayList) Programa.elMeuMagatzem.getClients());

				} else {

					initTableClients(tableClients, list1);

				}

			}

		});
		actiu.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (actiu.isSelected()) {
					if (c == null) {
					} else
						c.setActiu(true);
				} else
					c.setActiu(false);
			}

		});

		modify.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (!AJF.get(0).getText().equals("")) {
					c.setNomClient(AJF.get(0).getText());
				}
				if (!AJF.get(1).getText().equals("")) {
					c.setDireccio(AJF.get(1).getText());
				}
				if (!AJF.get(2).getText().equals("")) {
					c.setPoblacio(AJF.get(2).getText());
				}
				if (!AJF.get(3).getText().equals("")) {
					c.setPersonaContacte(AJF.get(3).getText());
				}
				if (!AJF.get(4).getText().equals("")) {
					c.setTelefon(AJF.get(4).getText());
				}
				initTableClients(tableClients, (ArrayList) Programa.elMeuMagatzem.getClients());
			}
		});

		btnModificar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				ModificarProducto mo = new ModificarProducto(p);
				mo.getFrame().setVisible(true);
				;

			}
		});
		// pesta�a comandas, tabla comandas
		tableComandes.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				mouseX = e.getX();
				mouseY = e.getY();

				int n = tableComandes.getSelectedRow();

				Object s = tableComandes.getValueAt(n, 0);

				for (Comanda p1 : Programa.elMeuMagatzem.getComandes()) {

					if (s.equals(p1.getIdComanda())) {

						co = p1;

						switch (co.getEstat().toString()) {

						case "PENDENT":

							Pendent.setSelected(true);
							Transport.setSelected(false);
							Preparada.setSelected(false);
							Lliurada.setSelected(false);

							break;

						case "TRANSPORT":

							Transport.setSelected(true);
							Preparada.setSelected(false);
							Pendent.setSelected(false);
							Lliurada.setSelected(false);

							break;

						case "LLIURADA":

							Lliurada.setSelected(true);
							Preparada.setSelected(false);
							Transport.setSelected(false);
							Pendent.setSelected(false);

							break;

						case "PREPARADA":

							Preparada.setSelected(true);
							Transport.setSelected(false);
							Pendent.setSelected(false);
							Lliurada.setSelected(false);
							break;

						}

						initTableLinia(tablaLinia, co);

					}

				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
		// pesta�a clientes, tabla clientes
		tableClients.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				int n = tableClients.getSelectedRow();

				Object s = tableClients.getValueAt(n, 0);

				for (Client cl : Programa.elMeuMagatzem.getClients()) {

					if (s.equals(cl.getIdClient())) {

						c = cl;
					}

				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		// pesta�a productes, tabla productes
		tableProducte.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {

				mouseX = e.getX();
				mouseY = e.getY();

				int n = tableProducte.getSelectedRow();
				
				
				Object s = tableProducte.getValueAt(n, 1);
				for (Producte p1 : Programa.elMeuMagatzem.getProductes()) {

					if (p1.getNomProducte().equals(s)) {

						p = p1;

						initTableLot(tableLot, p);

						initTableComponent(tableComponent, p);

					}

				}

			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});

		Preparada.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (Preparada.isSelected()) {
					Transport.setSelected(false);
					Pendent.setSelected(false);
					Lliurada.setSelected(false);
					co.setEstat(Enum.valueOf(ComandaEstat.class, "PREPARADA"));

				} else {

					co.setEstat(Enum.valueOf(ComandaEstat.class, Preparada.getText()));

				}

			}
		});

		Transport.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (Transport.isSelected()) {
					Pendent.setSelected(false);
					Lliurada.setSelected(false);
					Preparada.setSelected(false);
					co.setEstat(Enum.valueOf(ComandaEstat.class, "TRANSPORT"));

				} else {

					co.setEstat(Enum.valueOf(ComandaEstat.class, Preparada.getText()));

				}

			}
		});

		Lliurada.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (Lliurada.isSelected()) {
					Transport.setSelected(false);
					Pendent.setSelected(false);
					Preparada.setSelected(false);
					co.setEstat(Enum.valueOf(ComandaEstat.class, "LLIURADA"));

				} else {

					co.setEstat(Enum.valueOf(ComandaEstat.class, Preparada.getText()));

				}

			}
		});

		Pendent.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (Pendent.isSelected()) {

					Transport.setSelected(false);
					Lliurada.setSelected(false);
					Preparada.setSelected(false);
					Pendent.setSelected(false);

				} else {

					co.setEstat(Enum.valueOf(ComandaEstat.class, Preparada.getText()));

				}
			}
		});

		comanda_id.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String id = comanda_id.getText();

				ArrayList<Comanda> list = (ArrayList) Programa.elMeuMagatzem.getComandes();

				ArrayList<Comanda> list1 = new ArrayList<Comanda>();

				if (id.equals("")) {
				} else {
					int i = Integer.parseInt(id);

					for (int j = 0; j < list.size(); j++) {

						if (list.get(j).getIdComanda() == i) {

							list1.add(list.get(j));

						}

					}
				}

				if (list1.isEmpty()) {

					initTableComanda(tableComandes, (ArrayList) Programa.elMeuMagatzem.getComandes());

				} else {

					initTableComanda(tableComandes, list1);

				}

			}

		});

		textField_1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String id = textField_1.getText();

				ArrayList<Producte> list = (ArrayList) Programa.elMeuMagatzem.getProductes();

				ArrayList<Producte> list1 = new ArrayList<Producte>();

				if (id.equals("")) {
				} else {
					int i = Integer.parseInt(id);

					for (int j = 0; j < list.size(); j++) {

						if (list.get(j).getCodiProveidor() == i) {

							list1.add(list.get(j));

						}

					}
				}

				if (list1.isEmpty()) {

					initTableProducte(tableProducte, (ArrayList) Programa.elMeuMagatzem.getProductes());
					

				} else {

					initTableProducte(tableProducte, list1);

				}

			}

		});

		btnBorrar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				int n = tableProducte.getSelectedRow();

				Object s = tableProducte.getValueAt(n, 1);

				Producte p = null;

				for (Producte p1 : Programa.elMeuMagatzem.getProductes()) {

					if (p1.getNomProducte().equals(s)) {

						p = p1;

					}

				}

				Programa.elMeuMagatzem.getProductes().remove(p);


				initTableProducte(tableProducte, (ArrayList) Programa.elMeuMagatzem.getProductes());

			}
		});

	}

	private void initTableComanda(JTable tableComandes, ArrayList list) {

		ModelComandes model = new ModelComandes(list);

		tableComandes.setModel(model);

	}

	

	@Override
	public void actionPerformed(ActionEvent e) {

		String id = ProdNom.getText();

		ArrayList<Producte> list = (ArrayList) Programa.elMeuMagatzem.getProductes();

		ArrayList<Producte> list1 = new ArrayList<Producte>();

		for (int i = 0; i < list.size(); i++) {

			if (list.get(i).getNomProducte().equals(id)) {

				list1.add(list.get(i));

			}

		}

		if (list1.isEmpty()) {

			initTableProducte(tableProducte, (ArrayList) Programa.elMeuMagatzem.getProductes());

		} else {

			initTableProducte(tableProducte, list1);

		}

	}

	public static void initTableProducte(JTable table, ArrayList list) {

		ModelProductos model = new ModelProductos(list);

		table.setModel(model);

	}

	public static void initTableLot(JTable tableLot, Producte p) {

		ModelLot model = new ModelLot(p);

		tableLot.setModel(model);

	}

	public static void initTableComponent(JTable tableComponent, Producte p) {

		ModelComponent model = new ModelComponent(p);

		tableComponent.setModel(model);

	}

	public static void initTableClients(JTable table, ArrayList list) {

		ModelClient model = new ModelClient(list);

		table.setModel(model);

	}

	public void initTableLinia(JTable table, Comanda co) {

		ModelLinia model = new ModelLinia(co);

		table.setModel(model);

	}
}
