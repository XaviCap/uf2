package Graphics;

import java.util.Map;
import java.util.Map.Entry;

import javax.swing.table.AbstractTableModel;

import Tables.Producte;

public class ModelComponent extends AbstractTableModel {

	Producte pr;
	private final String[] tableHeaders = { "NomProducte", "Quantitat" };

	public ModelComponent(Producte p) {

		pr = p;

	}

	@Override
	public int getRowCount() {
		return pr.getComposicio().size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

			
			Entry<Producte,Integer> entry = (Entry<Producte, Integer>) pr.getComposicio().entrySet().toArray()[rowIndex];
				
				if (columnIndex == 0) {
					
					return entry.getKey().getNomProducte();
				} else
					return entry.getValue();
			
	}

	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}

}
