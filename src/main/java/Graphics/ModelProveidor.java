package Graphics;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Tables.Producte;
import Tables.Proveidor;

public class ModelProveidor extends AbstractTableModel {

	ArrayList al1 = null;
	private final String[] tableHeaders = { "Id Proveidor","Nom Proveidor","Adre�a"};
	
	public ModelProveidor(ArrayList al) {
		
		al1 = al;
		
	}
	
	@Override
	public int getRowCount() {
		return al1.size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 3;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		Proveidor entity = (Proveidor) al1.get(rowIndex);
		
		
		switch(columnIndex) {
		
		case 0: 
			
			return entity.getIdProveidor();
			
		case 1: 
			
			return entity.getNomProveidor();
			
		case 2: 
			
			return entity.getDireccio();
			
		default:
		
		}
		
		return null;
		
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}

}
