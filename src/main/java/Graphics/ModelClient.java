package Graphics;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Tables.Client;
import Tables.Producte;

public class ModelClient extends AbstractTableModel {
	ArrayList lista;
	private final String[] tableHeaders = { "idClient", "nomClient", "actiu","telefon","personaContacte", "direccio" };

	public ModelClient(ArrayList list) {
		lista = list;
	}

	@Override
	public int getColumnCount() {
		return 6;
	}

	@Override
	public int getRowCount() {
		return lista.size();
	}

	// this method is called to set the value of each cell
	@Override
	public Object getValueAt(int row, int column) {

		Client entity = null;
		entity = (Client) lista.get(row);

		switch (column) {

		case 0:

			return entity.getIdClient();

		case 1:

			return entity.getNomClient();
			
		case 2:
			
			return entity.isActiu();
			
		case 3: 
			
			return entity.getTelefon();
			
		case 4:
			
			return entity.getPersonaContacte();

		case 5:
			
			if(entity.getDireccio() == null) {
				
				return "null";
				
			}else {

			return entity.getDireccio();

			}
		default:

			return "";
		}
	}

	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}
}

