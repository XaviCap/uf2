package Graphics;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import Tables.*;

public class ModelLot extends AbstractTableModel{

	Producte pr;
	private final String[] tableHeaders = { "Lot", "Quantitat", "Data Entrada","Data Caducitat"};
	
	public ModelLot(Producte p) {
		pr = p;
	}
	
	@Override
	public int getRowCount() {
		return pr.getLots().size();
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 4;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {

		LotDesglossat entity = null;
		entity = (LotDesglossat) pr.getLots().get(rowIndex);

		switch (columnIndex) {
		
		case 0:
			
			return entity.getLot();
			
		case 1:
			
			return entity.getQuantitat();
			
		case 2: 
			
			return entity.getDataEntrada();
			
		case 3:
			
			return entity.getDataCaducitat();
			
		default:
		
		}

		
		return null;
	}
	
	@Override
	public String getColumnName(int columnIndex) {
		return tableHeaders[columnIndex];
	}

}
