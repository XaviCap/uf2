package Graphics;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import Tables.Generador;
import Tables.LotDesglossat;
import Tables.Producte;
import Tables.Programa;
import java.awt.Toolkit;

public class AfegirLot {

	/**
	 * Launch the application.
	 */
	private JFrame frmAfegirLot;

	public JFrame getFrame() {
		return frmAfegirLot;
	}

	public void setFrame(JFrame frame) {
		this.frmAfegirLot = frame;
	}

	private JTextField quantext;
	private JTextField cadutext;
	private JTable tableProveidor;
	int mouseX = 0;
	int mouseY = 0;
	private JTextField textField_4;


	public AfegirLot(Producte p) throws NullPointerException {
		try {
			initialize(p);

		} catch (Exception NullPointerException) {
			initialize(p);
		} finally {
			initialize(p);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize(Producte p) {
		
		frmAfegirLot = new JFrame();
		frmAfegirLot.setTitle("Afegir Lot");
		frmAfegirLot.setIconImage(Toolkit.getDefaultToolkit().getImage(AfegirLot.class.getResource("/com/sun/javafx/scene/web/skin/Paste_16x16_JFX.png")));
		frmAfegirLot.setBounds(100, 100, 469, 290);
		frmAfegirLot.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmAfegirLot.getContentPane().setLayout(null);

		quantext = new JTextField();
		quantext.setBounds(93, 11, 86, 20);
		frmAfegirLot.getContentPane().add(quantext);
		quantext.setColumns(10);

		cadutext = new JTextField();
		cadutext.setBounds(93, 42, 86, 20);
		frmAfegirLot.getContentPane().add(cadutext);
		cadutext.setColumns(10);

		JLabel quantlot = new JLabel("Quantitat");
		quantlot.setBounds(10, 14, 46, 14);
		frmAfegirLot.getContentPane().add(quantlot);

		JLabel lblNewLabel_1 = new JLabel("Data caducitat");
		lblNewLabel_1.setBounds(10, 45, 73, 14);
		frmAfegirLot.getContentPane().add(lblNewLabel_1);

		JButton addlot = new JButton("Afegir\r\n");
		addlot.setBounds(10, 198, 89, 23);
		frmAfegirLot.getContentPane().add(addlot);

		JButton tancalot = new JButton("Tancar");
		tancalot.setBounds(128, 198, 89, 23);
		frmAfegirLot.getContentPane().add(tancalot);

		addlot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy");
				String dateInString = cadutext.getText();
				Date date = null;
				try {
					date = sdf.parse(dateInString);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					System.out.println("No s'ha afegit el lot,la data utilitza el format dd/mm/yyyy");
				}
				LotDesglossat l = new LotDesglossat(Generador.getNextLot(), date, Integer.parseInt(quantext.getText()));
				
				
				p.afegirLot(l.getQuantitat(), l.getDataCaducitat());
				try {
					p.afegirLot(l.getQuantitat(), l.getDataCaducitat());
				} catch (Exception NullPointerException) {
					Programa.elMeuMagatzem.getProductes().get(0).afegirLot(l.getQuantitat(), l.getDataCaducitat());
				}

			}
		}

		);
		tancalot.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				frmAfegirLot.setVisible(false);
			}

		});
	}

}
